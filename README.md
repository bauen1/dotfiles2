# Setup

```sh
git clone "https://gitlab.com/bauen1/dotfiles2" "~/.dotfiles"
rm -f "${HOME}/.profile" "${HOME}/.bash_logout" "${HOME}/.bashrc"
stow --verbose --target="${HOME}" --dir="${HOME}/.dotfiles" --no-folding --stow profile bash
```

# Dependencies

* GNU Stow
