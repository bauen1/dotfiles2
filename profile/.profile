#!/bin/sh

PATH="${HOME}/bin:${HOME}/.local/bin:${PATH}"
export PATH

EDITOR="nano"
if command -v nvim >/dev/null; then
    EDITOR="nvim"
fi
export EDITOR

# fix ssh-agent launch behaviour for graphic and terminal sessions (see systemd service)
SSH_AUTH_SOCK="${XDG_RUNTIME_DIR:-/tmp/"$(id -u)"}/openssh_agent"
export SSH_AUTH_SOCK

# local modifications
if [ -f "$HOME/.profile.local" ]; then
    # shellcheck source=.profile.local
    . "$HOME/.profile.local"
fi
