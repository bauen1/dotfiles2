#!/bin/bash

# shellcheck source=.profile
. "$HOME/.profile"

# For interactive sessions also source bashrc
# shellcheck source=.bashrc
[[ $- == *i* ]] && . "$HOME/.bashrc"
