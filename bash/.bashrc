#!/bin/bash

# tab completion
# This increases load time by a lot
if [[ -f "/etc/bash_completion" ]]; then
    . "/etc/bash_completion"
fi

# fix tty settings if necessary
stty sane

# Update LINES, COLUMNS after every external command
shopt -s checkwinsize

## History:

# append instead of overwriting the history file
shopt -s histappend

# Ignore commands with a leading space or duplicate commands
export HISTCONTROL=ignoreboth

# No size limit and never truncate the history file
export HISTFILESIZE=-1
export HISTSIZE=-1

# Also record time of command
export HISTTIMEFORMAT='%F-%T%z'

## Colors

# from debians default bashrc
if [ -x "/usr/bin/tput" ] && tput setaf 1 >&/dev/null; then
    color_prompt=yes
else
    color_prompt=
fi

# TODO: different prompt when running as root, either via sudo -s or similiar

if [[ "$color_prompt" = "yes" ]]; then
    PS1='\[\033[01;32m\]\u@\H\[\033[0;0m\]:\[\033[01;34m\]\w\[\033[0;0m\] \$ '

    if command -v dircolors &>/dev/null; then
        eval "$(dircolors --bourne-shell)"
        alias ls='ls --human-readable --color=auto'
        alias dir='dir --color=auto'
        alias vdir='vdir --color=auto'
    fi

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'

    alias ip='ip --color=auto'

    SUDO_PROMPT="\033[1;31m[sudo] password for %p@%h:\033[0;0m"
else
    PS1='\u@\H:\w \$ '
    SUDO_PROMPT="[sudo] password for %p@%h:"
fi

export PS1
export SUDO_PROMPT

unset color_prompt

if command -v lesspipe &>/dev/null; then
    eval "$(lesspipe)"
fi

# Aliases
# shellcheck source=.bash_aliases
. "$HOME/.bash_aliases"

# local modifications
if [[ -f "$HOME/.bashrc.local" ]]; then
    # shellcheck source=.bashrc.local
    . "$HOME/.bashrc.local"
fi
