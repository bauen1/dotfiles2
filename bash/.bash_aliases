#!/bin/bash

# Bash Aliases

alias ll='ls -l'
alias la='ls -A'

# ask for confirmation before removing things
alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'

# local modifications
if [[ -f "$HOME/.bash_aliases.local" ]]; then
    # shellcheck source=.bashrc.local
    . "$HOME/.bash_aliases.local"
fi
