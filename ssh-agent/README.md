# ssh-agent

Supply a better systemd service than the default provided by debian.

```bash
# 1. Enable / start service
systemctl --user enable ssh-agent.service
systemctl --user start ssh-agent.service

# 2. Add ssh-keys
ssh-add
```
